package hu.ponte.hr;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import hu.ponte.hr.controller.ImagesController;
import hu.ponte.hr.controller.upload.UploadController;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SeniorTestApplicationTests {
	
	@Autowired
	private UploadController uploadController;

	@Autowired
	private ImagesController imagesController;

	
	@Test
	public void contexLoads() throws Exception {
		assertThat(uploadController).isNotNull();
	}
	
	@Test
	public void contexLoadsImagesController() throws Exception {
		assertThat(imagesController).isNotNull();
	}

}
