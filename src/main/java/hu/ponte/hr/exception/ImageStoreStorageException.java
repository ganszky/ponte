package hu.ponte.hr.exception;

public class ImageStoreStorageException extends RuntimeException {
	private static final long serialVersionUID = -2585502326974628539L;

	public ImageStoreStorageException(String message) {
        super(message);
    }

    public ImageStoreStorageException(String message, Throwable cause) {
        super(message, cause);
    }
}
