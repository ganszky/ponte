package hu.ponte.hr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.web.servlet.MultipartAutoConfiguration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableAsync
@SpringBootApplication(scanBasePackages = "hu.ponte.hr", exclude = { MultipartAutoConfiguration.class })
@EntityScan(basePackages = "hu.ponte.hr.model")
@EnableTransactionManagement
public class SeniorTestApplication {
	public static void main(String[] args) {
		SpringApplication.run(SeniorTestApplication.class, args);
	}

}
