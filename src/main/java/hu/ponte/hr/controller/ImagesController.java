package hu.ponte.hr.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hu.ponte.hr.model.ImageStoreEntity;
import hu.ponte.hr.services.ImageStoreService;
import lombok.extern.log4j.Log4j2;

@RestController()
@RequestMapping("api/images")
@Log4j2
public class ImagesController {

	@Autowired
	private ImageStoreService imageStoreService;

	@GetMapping("meta")
	public List<ImageMeta> listImages() {
		return imageStoreService.getImages();
	}

	@GetMapping("preview/{id}")
	public void getImage(@PathVariable("id") String id, HttpServletResponse response) {
		ImageStoreEntity imageStoreEntity = imageStoreService.findById(id);
		response.setContentType(MediaType.parseMediaType(imageStoreEntity.getMimeType()).toString());
		response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
				"attachment; filename=\"" + imageStoreEntity.getFileName() + "\"");

		InputStream in = new ByteArrayInputStream(imageStoreEntity.getData());
	    try {
			IOUtils.copy(in, response.getOutputStream());
		} catch (IOException e) {
			log.log(Level.DEBUG, e.getLocalizedMessage());			
		}	    
	}
}
