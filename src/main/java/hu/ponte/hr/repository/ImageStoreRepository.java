package hu.ponte.hr.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import hu.ponte.hr.controller.ImageMeta;
import hu.ponte.hr.model.ImageStoreEntity;

@Repository
public interface ImageStoreRepository extends JpaRepository<ImageStoreEntity, String> {

	@Query("SELECT new hu.ponte.hr.controller.ImageMeta(e.id, e.fileName, e.mimeType, e.size, e.digitalSign) FROM ImageStoreEntity e")
	public List<ImageMeta> findAllMeta();
}
