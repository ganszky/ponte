package hu.ponte.hr.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "image")
public class ImageStoreEntity {
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Getter
	@Setter
	private String id;
	@Getter
	@Setter
	private String fileName;
	@Getter
	@Setter
	private String mimeType;
	@Getter
	@Setter
	@Column(name = "digital_sign",length = 500)
	private String digitalSign;
	@Getter
	@Setter
	private Long size;
	@Lob
	@Getter
	@Setter
	private byte[] data;

	public ImageStoreEntity() {

	}

	public ImageStoreEntity(String fileName, String mimeType, byte[] data,String digitalSign, Long size) {
		this.fileName = fileName;
		this.mimeType = mimeType;
		this.data = data;
		this.digitalSign = digitalSign;
		this.size = size;
	}

}
