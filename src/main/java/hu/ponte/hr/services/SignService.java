package hu.ponte.hr.services;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

@Service
public class SignService {

    @Value("${signAlgorithm:SHA256withRSA}")
    private String signAlgorithm;

    @Value("${privateKeyFile:classpath:config/keys/key.private}")
    private String privateKeyFile;
    

	/**
	 * Aláírja a privát kulccsal az byte tömböt és visszaadja azt Base64Encode-olva.
	 * 
	 * @param data az aláírandó byte tömb Base64Encode-olva
	 * @return az aláírt byte tömb
	 * @throws InvalidKeyException 
	 * @throws Exception
	 */

    public String signAsBase64Encoded(byte[] data) throws InvalidKeyException, Exception {
    	return new String(Base64.getEncoder().encode(sign(data)), StandardCharsets.UTF_8);
    }

	/**
	 * Aláírja a privát kulccsal az byte tömböt.
	 * 
	 * @param data az aláírandó byte tömb
	 * @return az aláírt byte tömb
	 * @throws InvalidKeyException 
	 * @throws Exception
	 */
	public byte[] sign(byte[] data) throws InvalidKeyException, Exception {

		Signature dsa = Signature.getInstance(signAlgorithm); 
		dsa.initSign(getPrivateKey());
		dsa.update(data);
		return dsa.sign();
	}
	
	/**
	 * Visszaadja a privát kulcsot.
	 * 
	 * @return a privát kulcs
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 */
	public PrivateKey getPrivateKey() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException  {
		ResourceLoader resourceLoader = new DefaultResourceLoader();
		Resource resource = resourceLoader.getResource(privateKeyFile);
		byte[] keyBytes = Files.readAllBytes(resource.getFile().toPath());
		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePrivate(spec);
	}

}
