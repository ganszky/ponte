package hu.ponte.hr.services;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import hu.ponte.hr.controller.ImageMeta;
import hu.ponte.hr.exception.ImageNotFoundException;
import hu.ponte.hr.exception.ImageStoreStorageException;
import hu.ponte.hr.model.ImageStoreEntity;
import hu.ponte.hr.repository.ImageStoreRepository;
import lombok.extern.log4j.Log4j2;

@Service
@Transactional
@Log4j2
public class ImageStoreService {

	@Autowired
	private ImageStoreRepository imageStoreRepository;

	@Autowired
	private SignService signService;

	/**
	 * Elmenti a beérkezett file-t az adatbázisba - és alá is írja.
	 * 
	 * @param file a mentendő file
	 * @return az elmentett adatbázis entitás
	 */
	public ImageStoreEntity storeImage(MultipartFile file) {
		// Normalize file name
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());

		try {

			ImageStoreEntity dbFile = new ImageStoreEntity(fileName, file.getContentType(), file.getBytes(),
					signService.signAsBase64Encoded(file.getBytes()), file.getSize());

			return imageStoreRepository.save(dbFile);
		} catch (IOException ex) {
			log.error(ex);
			throw new ImageStoreStorageException("A file nem menthető! " + fileName + ". Kérem próbálja újra!", ex);
		} catch (InvalidKeyException e) {
			log.error(e);
			throw new ImageStoreStorageException("A file aláírása nem sikerült " + fileName + ". Kérem próbálja újra!",
					e);
		} catch (Exception e) {
			log.error(e);
			throw new ImageStoreStorageException(
					"Váratlan hiba a file mentésekor " + fileName + ". Kérem próbálja újra!", e);
		}

	}

	/**
	 * Visszadja az összes elmentett file (kép) meta adatait
	 * 
	 * @return az összes elmentett file meta adatainak listája
	 */
	public List<ImageMeta> getImages() {
		return imageStoreRepository.findAllMeta();
	}

	/**
	 * Visszadja az adott azonosítójú elmentett file-t annak metaadataival együtt.
	 * 
	 * @return az adott azonosítójú elmentett file (kép) annak metaadataival együtt.
	 */

	public ImageStoreEntity findById(String fileId) {
		return imageStoreRepository.findById(fileId)
				.orElseThrow(() -> new ImageNotFoundException("A file nem található! Azonosító: " + fileId));

	}

}
